#!/bin/sh
exec 1>&2  # send everything to log

srcdir="$(cat DIBS_DIR_SRC)"

apk update
apk add --no-cache nginx

rm -f /etc/nginx/conf.d/*
cp "$srcdir"/nginx.conf /etc/nginx
rm -rf /usr/share/nginx/html
mkdir -p /usr/share/nginx
tar cC "$srcdir" html | tar xC /usr/share/nginx
