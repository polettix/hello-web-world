# Hello, Web World!

A simple [Docker][] image for serving a simple web page. The container
listens on port `8080`:

```shell
$ docker run --rm -p 8080:8080 registry.gitlab.com/polettix/hello-web-world
```

The contents of this repository are licensed according to the Apache
License 2.0 (see file `LICENSE` in the project's root directory):

>  Copyright 2020 by Flavio Poletti
>
>  Licensed under the Apache License, Version 2.0 (the "License");
>  you may not use this file except in compliance with the License.
>  You may obtain a copy of the License at
>
>      http://www.apache.org/licenses/LICENSE-2.0
>
>  Unless required by applicable law or agreed to in writing, software
>  distributed under the License is distributed on an "AS IS" BASIS,
>  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
>  See the License for the specific language governing permissions and
>  limitations under the License.

*Check* image from [openclipart.org][] at
[https://openclipart.org/detail/312459/green-checkmark-✅](https://openclipart.org/detail/312459/green-checkmark-%E2%9C%85)
(public domain).

CSS reset from
[http://meyerweb.com/eric/tools/css/reset/](http://meyerweb.com/eric/tools/css/reset/)
(public domain).

[Docker]: https://www.docker.com/
[openclipart.org]: https://openclipart.org/
